'use strict';

angular.module('reports').config(['$stateProvider',
    function($stateProvider) {
        // Reports state routing
        $stateProvider.
			state('listReports', {
            url: '/reports',
            templateUrl: 'modules/reports/views/reports.client.view.html'
        });
    }
]);
