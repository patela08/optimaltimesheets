/**
 * Created by abpatel on 1/25/2017.
 */
'use strict';

// Configuring the Articles module
angular.module('reports').run(['Menus',
    function(Menus) {
        // Set top bar menu items
        Menus.addMenuItem('topbar', 'Reports', 'reports', 'dropdown', '/reports(/create)?',false,['admin']);
        Menus.addSubMenuItem('topbar', 'reports', 'Timesheet Reports', 'reports');
    }
]);
