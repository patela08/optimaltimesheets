/**
 * Created by abpatel on 1/25/2017.
 */
'use strict';

//Reports service used to communicate Reports REST endpoints
angular.module('reports').factory('Reports', ['$resource',
    function($resource) {
        return $resource('reports/:reportId', { reportId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
