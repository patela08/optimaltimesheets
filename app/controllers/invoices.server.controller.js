'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Invoice = mongoose.model('Invoice'),
	async = require('async'),
	_ = require('lodash'),
	fs = require('fs'),
    pdf = require('html-pdf'),
	nodemailer = require('nodemailer');
var config = require('../../config/config');
var smtpTransport = nodemailer.createTransport(config.mailer.options);
/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'Invoice already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};

/**
 * Create a Invoice
 */
exports.create = function(req, res) {
	var invoice = new Invoice(req.body);
	invoice.user = req.user;

	invoice.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(invoice);
		}
	});
};

/**
 * Show the current Invoice
 */
exports.read = function(req, res) {
	res.jsonp(req.invoice);
};

/**
 * Update a Invoice
 */
exports.update = function(req, res) {
	var invoice = req.invoice ;

	invoice = _.extend(invoice , req.body);

	invoice.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(invoice);
		}
	});
};

/**
 * Delete an Invoice
 */
exports.delete = function(req, res) {
	var invoice = req.invoice ;

	invoice.remove(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(invoice);
		}
	});
};

/**
 * List of Invoices
 */
exports.list = function(req, res) { Invoice.find().sort('-created').populate('user', 'displayName').exec(function(err, invoices) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(invoices);
		}
	});
};

/**
 * Invoice middleware
 */
exports.invoiceByID = function(req, res, next, id) { Invoice.findById(id).populate('user', 'displayName').exec(function(err, invoice) {
		if (err) return next(err);
		if (! invoice) return next(new Error('Failed to load Invoice ' + id));
		req.invoice = invoice ;
		next();
	});
};

/**
 * Invoice authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.invoice.user.id !== req.user.id) {
		return res.send(403, 'User is not authorized');
	}
	next();
};

/**
 * Preview of invoice based on postdata
 * /
exports.preview = function(req,res){
	res.render('invoice/invoiceTmpl', {
		user: req.user,
		invoice:req.body.invoice,
		company:req.body.company
	});
};
*/
exports.preview=function(req,res){

	var invoice = req.body.invoice;
	var company = req.body.company;
	var action = req.body.action;

	async.waterfall([
		// Render HTML based on data
		function(done) {
			res.render('invoice/invoiceTmpl', {
				user: req.user,
				invoice:invoice,
				company:company
			},function(err,htmlData){
				if(err){
					return res.status(400).send({
						message: 'Error while creating preview',
						err:err
					});
				}
				if(!action){
					return res.send(htmlData);
				}
				done(err,htmlData);
			});
		},
		// Juice to inline CSS

		// Save Invoice in DB
		function(htmlData,done){
			Invoice.findById(invoice._id).exec(function(err, inv) {
				if(inv){
					invoice = _.extend(inv, invoice);
				} else {
					invoice = new Invoice(invoice);
				}
				var status = {status:'saved',effectiveDate:new Date()};
				if(action === 'saveAndSend'){
					status = {status:'Sent',effectiveDate:new Date()};
				}
				if(invoice.status){
					invoice.status.push(status);
				} else {
					invoice.status = [status];
				}
				invoice.save(function(err) {
					if(err){
						return res.status(400).send({
							message: 'Error While saving invoice',
							err:err
						});
					}
					if(action === 'save'){
						res.jsonp({success:true});
					} else if(action === 'saveAndSend'){
						done(err,htmlData);
					} else {
						res.jsonp({success:true,message:'Unknown Action'});
					}

				});
			});
		},

		// Create PDF Buffer
		function(htmlData,done){
			pdf.create(htmlData, {height: '11in',width: '8.5in',header: {height: '20mm'}},function(err, buffer) {
				console.log('PDF created');
				if(err){
					return res.status(400).send({
						message: 'Error While creating PDF',
						err:err
					});
				} else {
					console.log('pdf success');
					done(err,buffer,htmlData);
				}
			});
		},

		// Sending pdf for download
		function (buffer,htmlData,done){
			var fs = require('fs');
			fs.writeFile('public/tmp/'+invoice.name+'.pdf', buffer, function(err) {
				if(err) {
					return res.status(400).send({
						message: 'Error While creating PDF',
						err:err
					});
				} else {
					res.jsonp({success:true, invoiceURL: '/tmp/'+invoice.name+'.pdf'});
					setTimeout(function(){
						fs.unlink('public/tmp/'+invoice.name+'.pdf');
					},60000);
				}
			});
		},

		// SEND EMAIL IS DISABLED NOW so no done called from previous function
		//Send Email
		function(buffer,htmlData,done){
			var mailOptions = {
				to: invoice.sentTo,
				bcc: req.user.email+', '+company.email+', '+config.mailer.from,
				from: config.mailer.from,
				subject: 'Invoice For ' + invoice.consultantName,
				html: htmlData,
				attachments: [{
					filename: invoice.name+'.pdf',
					content: buffer
				}]
			};
			smtpTransport.sendMail(mailOptions, function(err) {
				if (!err) {
					res.jsonp({success:true});
				} else {
					return res.status(400).send({
						message: 'Invoice saved but there is an error while sending',
						err:err
					});
				}
			});
		},
		function (err, result) {
		}
	]);
};


exports.changeStatus = function(req,res){
	var invoice = req.invoice ;

	invoice.status.push({
		status:req.body.status,
		effectiveDate: new Date(),
		user: req.user.displayName
	});

	invoice.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(invoice);
		}
	});
};
exports.byProject = function(req,res){
	Invoice.find({project:req.params.project_id},'name fromDate toDate total').sort('-name').limit(5).lean(true).exec(function(err,result){
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(result);
		}
	});
};