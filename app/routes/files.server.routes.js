'use strict';
module.exports = function(app) {
	// Root routing
	var fileUpload = require('../../app/modules/file.upload');
	app.route('/fileUpload/downloadZip').post(fileUpload.downloadZip);
};